package diiicalc

import (
	"net/http"
)

const standardUrlValueOff = "off"
const standardUrlValueOn = "on"

type SkillChoice interface {
	ModifyDerivedStats(derivedStats *DerivedStats)
	PrintHtml(w http.ResponseWriter)
	GetValue() string
	SetValue(value string)
	GetUrlKey() string
	GetSkillSlug() string
	GetRuneSlugs() []string
}

func InitializeSkillChoice(s SkillChoice, r *http.Request) {

	// If there is already a value for this SkillChoice's URL key, then continue using it!
	urlValue := r.FormValue(s.GetUrlKey())

	if urlValue != "" {
		s.SetValue(urlValue)
		return
	}

	// If not, default the skill to off.
	s.SetValue(standardUrlValueOff)

	// If there is no value for this SkillChoice's URL key, then we must parse the 
	// skills from the user's build to see if this skill is being used or not.

	// First, check passive skills.
	for i := 0; i < len(urlKeysPassiveSkills); i++ {
		if s.GetSkillSlug() == r.FormValue(urlKeysPassiveSkills[i]) {
			s.SetValue(standardUrlValueOn)
			return
		}
	}

	// Now, check active skills and runes.
	for i := 0; i < len(urlKeysActiveSkills); i++ {

		if s.GetSkillSlug() != r.FormValue(urlKeysActiveSkills[i]) {
			continue
		}

		// If we got to this point, that means we matched up a user's active skill choice
		// in their build to a SkillChoice we recognize for our calculator.

		// Start by defaulting the skill to 'on'.
		s.SetValue(standardUrlValueOn)

		// Furthermore, check if we have a match on any of the runes we care about.
		userRuneSlug := r.FormValue(urlKeysActiveRunes[i])
		supportedRuneSlugs := s.GetRuneSlugs()

		for j := 0; j < len(supportedRuneSlugs); j++ {
			// If we have a match, set the URL value for this SkillChoice to match
			// the given rune slug. 
			if userRuneSlug == supportedRuneSlugs[j] {
				s.SetValue(userRuneSlug)
				return
			}
		}
	}
}

func GetSelected(s SkillChoice, urlValue string) (retVal string) {
	if s.GetValue() == urlValue {
		retVal = "selected"
	} else {
		retVal = ""
	}

	return
}

func ParseSkillChoices(r *http.Request) (skillChoices []SkillChoice) {

	skillChoices = make([]SkillChoice, 0, 10)

	heroClass := r.FormValue(urlKeyHeroClass)

	if heroClass == urlValueHeroClassBarbarian {

		toughAsNailsSkillChoice := new(ToughAsNailsSkillChoice)
		nervesOfSteelSkillChoice := new(NervesOfSteelSkillChoice)
		warCrySkillChoice := new(WarCrySkillChoice)
		leapSkillChoice := new(LeapSkillChoice)
		ignorePainSkillChoice := new(IgnorePainSkillChoice)

		InitializeSkillChoice(toughAsNailsSkillChoice, r)
		InitializeSkillChoice(nervesOfSteelSkillChoice, r)
		InitializeSkillChoice(warCrySkillChoice, r)
		InitializeSkillChoice(leapSkillChoice, r)
		InitializeSkillChoice(ignorePainSkillChoice, r)

		skillChoices = append(skillChoices, toughAsNailsSkillChoice)
		skillChoices = append(skillChoices, nervesOfSteelSkillChoice)
		skillChoices = append(skillChoices, warCrySkillChoice)
		skillChoices = append(skillChoices, leapSkillChoice)
		skillChoices = append(skillChoices, ignorePainSkillChoice)

	} else if heroClass == urlValueHeroClassMonk {

		seizeTheInitiativeSkillChoice := new(SeizeTheInitiativeSkillChoice)
		oneWithEverythingSkillChoice := new(OneWithEverythingSkillChoice)
		deadlyReachSkillChoice := new(DeadlyReachSkillChoice)
		mantraOfEvasionSkillChoice := new(MantraOfEvasionSkillChoice)
		mantraOfHealingSkillChoice := new(MantraOfHealingSkillChoice)

		InitializeSkillChoice(seizeTheInitiativeSkillChoice, r)
		InitializeSkillChoice(oneWithEverythingSkillChoice, r)
		InitializeSkillChoice(deadlyReachSkillChoice, r)
		InitializeSkillChoice(mantraOfEvasionSkillChoice, r)
		InitializeSkillChoice(mantraOfHealingSkillChoice, r)

		skillChoices = append(skillChoices, seizeTheInitiativeSkillChoice)
		skillChoices = append(skillChoices, oneWithEverythingSkillChoice)
		skillChoices = append(skillChoices, deadlyReachSkillChoice)
		skillChoices = append(skillChoices, mantraOfEvasionSkillChoice)
		skillChoices = append(skillChoices, mantraOfHealingSkillChoice)

	} else if heroClass == urlValueHeroClassWizard {

		energyArmorSkillChoice := new(EnergyArmorSkillChoice)
		glassCannonSkillChoice := new(GlassCannonSkillChoice)

		InitializeSkillChoice(energyArmorSkillChoice, r)
		InitializeSkillChoice(glassCannonSkillChoice, r)

		skillChoices = append(skillChoices, energyArmorSkillChoice)
		skillChoices = append(skillChoices, glassCannonSkillChoice)

	} else if heroClass == urlValueHeroClassDemonHunter {

	} else if heroClass == urlValueHeroClassWitchDoctor {

		horrifySkillChoice := new(HorrifySkillChoice)
		jungleFortitudeSkillChoice := new(JungleFortitudeSkillChoice)

		InitializeSkillChoice(horrifySkillChoice, r)
		InitializeSkillChoice(jungleFortitudeSkillChoice, r)

		skillChoices = append(skillChoices, horrifySkillChoice)
		skillChoices = append(skillChoices, jungleFortitudeSkillChoice)

	}

	return skillChoices
}
