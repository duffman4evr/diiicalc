package diiicalc

import (
	"fmt"
	"net/http"
)

const warCryUrlKey = "warCry"
const warCrySkillSlug = "war-cry"

// 0 -> Hardened Wrath
// 1 -> Impunity
var warCryRuneSlugs = []string{"war-cry-a", "war-cry-c"}

type WarCrySkillChoice struct {
	Value string
}

func (s *WarCrySkillChoice) GetValue() string {
	return s.Value
}

func (s *WarCrySkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *WarCrySkillChoice) GetUrlKey() string {
	return warCryUrlKey
}

func (s *WarCrySkillChoice) GetSkillSlug() string {
	return warCrySkillSlug
}

func (s *WarCrySkillChoice) GetRuneSlugs() []string {
	return warCryRuneSlugs
}

func (s *WarCrySkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	switch {
	case s.Value == standardUrlValueOn:
		derivedStats.Armor *= 1.2

	case s.Value == warCryRuneSlugs[0]:
		derivedStats.Armor *= 1.4

	case s.Value == warCryRuneSlugs[1]:
		derivedStats.Armor *= 1.2
		derivedStats.ResistFire *= 1.5
		derivedStats.ResistLightning *= 1.5
		derivedStats.ResistPoison *= 1.5
		derivedStats.ResistCold *= 1.5
		derivedStats.ResistArcane *= 1.5
		derivedStats.ResistPhysical *= 1.5

	}
}

func (s *WarCrySkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">War Cry:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Hardened Wrath</option>%s`, warCryRuneSlugs[0], GetSelected(s, warCryRuneSlugs[0]), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Impunity</option>%s`, warCryRuneSlugs[1], GetSelected(s, warCryRuneSlugs[1]), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
