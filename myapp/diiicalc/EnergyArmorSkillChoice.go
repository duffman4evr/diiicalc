package diiicalc

import (
	"fmt"
	"net/http"
)

const energyArmorUrlKey = "energyArmor"
const energyArmorSkillSlug = "energy-armor"

var energyArmorRuneSlugs = []string{"energy-armor-a"}

type EnergyArmorSkillChoice struct {
	Value string
}

func (s *EnergyArmorSkillChoice) GetValue() string {
	return s.Value
}

func (s *EnergyArmorSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *EnergyArmorSkillChoice) GetUrlKey() string {
	return energyArmorUrlKey
}

func (s *EnergyArmorSkillChoice) GetSkillSlug() string {
	return energyArmorSkillSlug
}

func (s *EnergyArmorSkillChoice) GetRuneSlugs() []string {
	return energyArmorRuneSlugs
}

func (s *EnergyArmorSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	switch {
	case s.Value == standardUrlValueOn:
		derivedStats.Armor *= 1.65

	case s.Value == energyArmorRuneSlugs[0]:
		derivedStats.Armor *= 1.65
		derivedStats.ResistArcane *= 1.4
		derivedStats.ResistFire *= 1.4
		derivedStats.ResistLightning *= 1.4
		derivedStats.ResistPoison *= 1.4
		derivedStats.ResistCold *= 1.4
		derivedStats.ResistPhysical *= 1.4
	}
}

func (s *EnergyArmorSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Energy Armor:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Prismatic</option>%s`, energyArmorRuneSlugs[0], GetSelected(s, energyArmorRuneSlugs[0]), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
