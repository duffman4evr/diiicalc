package diiicalc

import (
	"fmt"
	"net/http"
)

const toughAsNailsUrlKey = "toughAsNails"
const toughAsNailsSkillSlug = "tough-as-nails"

var toughAsNailsRuneSlugs = []string{}

type ToughAsNailsSkillChoice struct {
	Value string
}

func (s *ToughAsNailsSkillChoice) GetValue() string {
	return s.Value
}

func (s *ToughAsNailsSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *ToughAsNailsSkillChoice) GetUrlKey() string {
	return toughAsNailsUrlKey
}

func (s *ToughAsNailsSkillChoice) GetSkillSlug() string {
	return toughAsNailsSkillSlug
}

func (s *ToughAsNailsSkillChoice) GetRuneSlugs() []string {
	return toughAsNailsRuneSlugs
}

func (s *ToughAsNailsSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == standardUrlValueOn {
		derivedStats.Armor *= 1.25
	}
}

func (s *ToughAsNailsSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Tough as Nails:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
