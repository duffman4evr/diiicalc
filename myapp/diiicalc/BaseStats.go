package diiicalc

import (
	"net/http"
	"strconv"
)

// Some useful types
type BaseStats struct {
	HeroClass       string
	Level           float64
	Dexterity       int64
	Vitality        int64
	Armor           float64
	Life            float64
	LifeOnHit       float64
	LifeRegen       float64
	ResistArcane    float64
	ResistFire      float64
	ResistLightning float64
	ResistPoison    float64
	ResistCold      float64
	ResistPhysical  float64
}

func NewBaseStats(r *http.Request) *BaseStats {

	ip := new(BaseStats)

	// Parse in all the source values from the request.
	ip.HeroClass = r.FormValue(urlKeyHeroClass)
	ip.Level, _ = strconv.ParseFloat(r.FormValue(urlKeyLevel), 64)
	ip.Dexterity, _ = strconv.ParseInt(r.FormValue(urlKeyDexterity), 10, 64)
	ip.Vitality, _ = strconv.ParseInt(r.FormValue(urlKeyVitality), 10, 64)
	ip.Armor, _ = strconv.ParseFloat(r.FormValue(urlKeyArmor), 64)
	ip.Life, _ = strconv.ParseFloat(r.FormValue(urlKeyLife), 64)
	ip.LifeOnHit, _ = strconv.ParseFloat(r.FormValue(urlKeyLifeOnHit), 64)
	ip.LifeRegen, _ = strconv.ParseFloat(r.FormValue(urlKeyLifeRegen), 64)
	ip.ResistArcane, _ = strconv.ParseFloat(r.FormValue(urlKeyResistArcane), 64)
	ip.ResistFire, _ = strconv.ParseFloat(r.FormValue(urlKeyResistFire), 64)
	ip.ResistLightning, _ = strconv.ParseFloat(r.FormValue(urlKeyResistLightning), 64)
	ip.ResistPoison, _ = strconv.ParseFloat(r.FormValue(urlKeyResistPoison), 64)
	ip.ResistCold, _ = strconv.ParseFloat(r.FormValue(urlKeyResistCold), 64)
	ip.ResistPhysical, _ = strconv.ParseFloat(r.FormValue(urlKeyResistPhysical), 64)

	return ip
}
