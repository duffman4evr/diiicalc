package diiicalc

import (
	"fmt"
	"net/http"
)

const mantraOfHealingUrlKey = "mantraOfHealing"
const mantraOfHealingSkillSlug = "mantra-of-healing"

var mantraOfHealingRuneSlugs = []string{"mantra-of-healing-e"}

type MantraOfHealingSkillChoice struct {
	Value string
}

func (s *MantraOfHealingSkillChoice) GetValue() string {
	return s.Value
}

func (s *MantraOfHealingSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *MantraOfHealingSkillChoice) GetUrlKey() string {
	return mantraOfHealingUrlKey
}

func (s *MantraOfHealingSkillChoice) GetSkillSlug() string {
	return mantraOfHealingSkillSlug
}

func (s *MantraOfHealingSkillChoice) GetRuneSlugs() []string {
	return mantraOfHealingRuneSlugs
}

func (s *MantraOfHealingSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == mantraOfHealingRuneSlugs[0] {
		derivedStats.ResistArcane *= 1.2
		derivedStats.ResistFire *= 1.2
		derivedStats.ResistLightning *= 1.2
		derivedStats.ResistPoison *= 1.2
		derivedStats.ResistCold *= 1.2
		derivedStats.ResistPhysical *= 1.2
	}
}

func (s *MantraOfHealingSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Mantra of Healing:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Time of Need</option>%s`, mantraOfHealingRuneSlugs[0], GetSelected(s, mantraOfHealingRuneSlugs[0]), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
