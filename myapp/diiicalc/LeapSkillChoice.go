package diiicalc

import (
	"fmt"
	"net/http"
)

const leapUrlKey = "leap"
const leapSkillSlug = "leap"

// 0 -> Iron Impact
var leapRuneSlugs = []string{"leap-d"}

type LeapSkillChoice struct {
	Value string
}

func (s *LeapSkillChoice) GetValue() string {
	return s.Value
}

func (s *LeapSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *LeapSkillChoice) GetUrlKey() string {
	return leapUrlKey
}

func (s *LeapSkillChoice) GetSkillSlug() string {
	return leapSkillSlug
}

func (s *LeapSkillChoice) GetRuneSlugs() []string {
	return leapRuneSlugs
}

func (s *LeapSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == leapRuneSlugs[0] {
		derivedStats.Armor *= 3
	}
}

func (s *LeapSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Leap:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Iron Impact</option>%s`, leapRuneSlugs[0], GetSelected(s, leapRuneSlugs[0]), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
