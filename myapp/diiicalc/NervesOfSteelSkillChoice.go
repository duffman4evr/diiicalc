package diiicalc

import (
	"fmt"
	"net/http"
)

const nervesOfSteelUrlKey = "nervesOfSteel"
const nervesOfSteelSkillSlug = "nerves-of-steel"

var nervesOfSteelRuneSlugs = []string{}

type NervesOfSteelSkillChoice struct {
	Value string
}

func (s *NervesOfSteelSkillChoice) GetValue() string {
	return s.Value
}

func (s *NervesOfSteelSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *NervesOfSteelSkillChoice) GetUrlKey() string {
	return nervesOfSteelUrlKey
}

func (s *NervesOfSteelSkillChoice) GetSkillSlug() string {
	return nervesOfSteelSkillSlug
}

func (s *NervesOfSteelSkillChoice) GetRuneSlugs() []string {
	return nervesOfSteelRuneSlugs
}

func (s *NervesOfSteelSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == standardUrlValueOn {
		derivedStats.Armor += float64(derivedStats.Vitality)
	}
}

func (s *NervesOfSteelSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Nerves of Steel:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
