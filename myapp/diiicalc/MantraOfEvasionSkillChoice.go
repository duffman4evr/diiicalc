package diiicalc

import (
	"fmt"
	"net/http"
)

const mantraOfEvasionUrlKey = "mantraOfEvasion"
const mantraOfEvasionSkillSlug = "mantra-of-evasion"

var mantraOfEvasionRuneSlugs = []string{"mantra-of-evasion-e"}

type MantraOfEvasionSkillChoice struct {
	Value string
}

func (s *MantraOfEvasionSkillChoice) GetValue() string {
	return s.Value
}

func (s *MantraOfEvasionSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *MantraOfEvasionSkillChoice) GetUrlKey() string {
	return mantraOfEvasionUrlKey
}

func (s *MantraOfEvasionSkillChoice) GetSkillSlug() string {
	return mantraOfEvasionSkillSlug
}

func (s *MantraOfEvasionSkillChoice) GetRuneSlugs() []string {
	return mantraOfEvasionRuneSlugs
}

func (s *MantraOfEvasionSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == mantraOfEvasionRuneSlugs[0] {
		derivedStats.Armor *= 1.2
	}
}

func (s *MantraOfEvasionSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Mantra of Evasion:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Hard Target</option>%s`, mantraOfEvasionRuneSlugs[0], GetSelected(s, mantraOfEvasionRuneSlugs[0]), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
