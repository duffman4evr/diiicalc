package diiicalc

import (
	"fmt"
	"net/http"
)

const glassCannonUrlKey = "glassCannon"
const glassCannonSkillSlug = "glass-cannon"

var glassCannonRuneSlugs = []string{}

type GlassCannonSkillChoice struct {
	Value string
}

func (s *GlassCannonSkillChoice) GetValue() string {
	return s.Value
}

func (s *GlassCannonSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *GlassCannonSkillChoice) GetUrlKey() string {
	return glassCannonUrlKey
}

func (s *GlassCannonSkillChoice) GetSkillSlug() string {
	return glassCannonSkillSlug
}

func (s *GlassCannonSkillChoice) GetRuneSlugs() []string {
	return glassCannonRuneSlugs
}

func (s *GlassCannonSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == standardUrlValueOn {
		derivedStats.Armor *= .9
		derivedStats.ResistArcane *= .9
		derivedStats.ResistFire *= .9
		derivedStats.ResistLightning *= .9
		derivedStats.ResistPoison *= .9
		derivedStats.ResistCold *= .9
		derivedStats.ResistPhysical *= .9
	}
}

func (s *GlassCannonSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Glass Cannon:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
