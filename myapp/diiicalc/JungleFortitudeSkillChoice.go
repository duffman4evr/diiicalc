package diiicalc

import (
	"fmt"
	"net/http"
)

const jungleFortitudeUrlKey = "jungleFortitude"
const jungleFortitudeSkillSlug = "jungle-fortitude"

var jungleFortitudeRuneSlugs = []string{}

type JungleFortitudeSkillChoice struct {
	Value string
}

func (s *JungleFortitudeSkillChoice) GetValue() string {
	return s.Value
}

func (s *JungleFortitudeSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *JungleFortitudeSkillChoice) GetUrlKey() string {
	return jungleFortitudeUrlKey
}

func (s *JungleFortitudeSkillChoice) GetSkillSlug() string {
	return jungleFortitudeSkillSlug
}

func (s *JungleFortitudeSkillChoice) GetRuneSlugs() []string {
	return jungleFortitudeRuneSlugs
}

func (s *JungleFortitudeSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == standardUrlValueOn {
		derivedStats.MitigationSources = append(derivedStats.MitigationSources, MitigationSource{"Jungle Fortitude", 0.20})
	}
}

func (s *JungleFortitudeSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Jungle Fortitude:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
