package diiicalc

import (
	"fmt"
	"net/http"
)

const deadlyReachUrlKey = "deadlyReach"
const deadlyReachSkillSlug = "deadly-reach"

var deadlyReachRuneSlugs = []string{"deadly-reach-e"}

type DeadlyReachSkillChoice struct {
	Value string
}

func (s *DeadlyReachSkillChoice) GetValue() string {
	return s.Value
}

func (s *DeadlyReachSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *DeadlyReachSkillChoice) GetUrlKey() string {
	return deadlyReachUrlKey
}

func (s *DeadlyReachSkillChoice) GetSkillSlug() string {
	return deadlyReachSkillSlug
}

func (s *DeadlyReachSkillChoice) GetRuneSlugs() []string {
	return deadlyReachRuneSlugs
}

func (s *DeadlyReachSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == deadlyReachRuneSlugs[0] {
		derivedStats.Armor *= 1.5
	}
}

func (s *DeadlyReachSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Deadly Reach:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Keen Eye</option>%s`, deadlyReachRuneSlugs[0], GetSelected(s, deadlyReachRuneSlugs[0]), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
