package diiicalc

import (
	"fmt"
	"net/http"
)

const oneWithEverythingUrlKey = "oneWithEverything"
const oneWithEverythingSkillSlug = "one-with-everything"

var oneWithEverythingRuneSlugs = []string{}

type OneWithEverythingSkillChoice struct {
	Value string
}

func (s *OneWithEverythingSkillChoice) GetValue() string {
	return s.Value
}

func (s *OneWithEverythingSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *OneWithEverythingSkillChoice) GetUrlKey() string {
	return oneWithEverythingUrlKey
}

func (s *OneWithEverythingSkillChoice) GetSkillSlug() string {
	return oneWithEverythingSkillSlug
}

func (s *OneWithEverythingSkillChoice) GetRuneSlugs() []string {
	return oneWithEverythingRuneSlugs
}

func (s *OneWithEverythingSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == standardUrlValueOn {
		max := findMax(derivedStats.ResistArcane, derivedStats.ResistFire, derivedStats.ResistLightning, derivedStats.ResistPoison, derivedStats.ResistCold, derivedStats.ResistPhysical)
		derivedStats.ResistArcane = max
		derivedStats.ResistFire = max
		derivedStats.ResistLightning = max
		derivedStats.ResistPoison = max
		derivedStats.ResistCold = max
		derivedStats.ResistPhysical = max
	}
}

func (s *OneWithEverythingSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">One With Everything:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
