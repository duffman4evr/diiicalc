package diiicalc

import (
	"fmt"
	"net/http"
)

const horrifyUrlKey = "horrify"
const horrifySkillSlug = "horrify"

// 0 -> Frightening Aspect
var horrifyRuneSlugs = []string{"horrify-a"}

type HorrifySkillChoice struct {
	Value string
}

func (s *HorrifySkillChoice) GetValue() string {
	return s.Value
}

func (s *HorrifySkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *HorrifySkillChoice) GetUrlKey() string {
	return horrifyUrlKey
}

func (s *HorrifySkillChoice) GetSkillSlug() string {
	return horrifySkillSlug
}

func (s *HorrifySkillChoice) GetRuneSlugs() []string {
	return horrifyRuneSlugs
}

func (s *HorrifySkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == horrifyRuneSlugs[0] {
		derivedStats.Armor *= 2
	}
}

func (s *HorrifySkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Horrify:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Frightening Aspect</option>%s`, horrifyRuneSlugs[0], GetSelected(s, horrifyRuneSlugs[0]), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
