package diiicalc

import (
	"fmt"
	"net/http"
)

const ignorePainUrlKey = "ignorePain"
const ignorePainSkillSlug = "ignore-pain"

var ignorePainRuneSlugs = []string{}

type IgnorePainSkillChoice struct {
	Value string
}

func (s *IgnorePainSkillChoice) GetValue() string {
	return s.Value
}

func (s *IgnorePainSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *IgnorePainSkillChoice) GetUrlKey() string {
	return ignorePainUrlKey
}

func (s *IgnorePainSkillChoice) GetSkillSlug() string {
	return ignorePainSkillSlug
}

func (s *IgnorePainSkillChoice) GetRuneSlugs() []string {
	return ignorePainRuneSlugs
}

func (s *IgnorePainSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == standardUrlValueOn {
		derivedStats.MitigationSources = append(derivedStats.MitigationSources, MitigationSource{"Ignore Pain", 0.65})
	}
}

func (s *IgnorePainSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Ignore Pain:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
