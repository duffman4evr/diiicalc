package diiicalc

import (
	"fmt"
	"net/http"
)

const siezeTheInitiativeUrlKey = "siezeTheInitiative"
const siezeTheInitiativeSkillSlug = "sieze-the-initiative"

var siezeTheInitiativeRuneSlugs = []string{}

type SeizeTheInitiativeSkillChoice struct {
	Value string
}

func (s *SeizeTheInitiativeSkillChoice) GetValue() string {
	return s.Value
}

func (s *SeizeTheInitiativeSkillChoice) SetValue(value string) {
	s.Value = value
}

func (s *SeizeTheInitiativeSkillChoice) GetUrlKey() string {
	return siezeTheInitiativeUrlKey
}

func (s *SeizeTheInitiativeSkillChoice) GetSkillSlug() string {
	return siezeTheInitiativeSkillSlug
}

func (s *SeizeTheInitiativeSkillChoice) GetRuneSlugs() []string {
	return siezeTheInitiativeRuneSlugs
}

func (s *SeizeTheInitiativeSkillChoice) ModifyDerivedStats(derivedStats *DerivedStats) {
	if s.Value == standardUrlValueOn {
		derivedStats.Armor += float64(derivedStats.Dexterity)
	}
}

func (s *SeizeTheInitiativeSkillChoice) PrintHtml(w http.ResponseWriter) {

	fmt.Fprintln(w, `<tr>`)
	fmt.Fprintln(w, `<td class="tableLeft">Seize the Initiative:</td>`)
	fmt.Fprintln(w, `<td class="tableRight">`)
	fmt.Fprintf(w, `<select name="%s" onchange="document.getElementById('defensiveForm').submit();">%s`, s.GetUrlKey(), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >Off</option>%s`, standardUrlValueOff, GetSelected(s, standardUrlValueOff), "\n")
	fmt.Fprintf(w, `<option value="%s" %s >On</option>%s`, standardUrlValueOn, GetSelected(s, standardUrlValueOn), "\n")
	fmt.Fprintln(w, `</select>`)
	fmt.Fprintln(w, `</td>`)
	fmt.Fprintln(w, `</tr>`)

}
