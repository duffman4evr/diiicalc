RELEASE NOTES

VERSION 1.0:

===UPDATES===

- Initial release. Ability to query the D3 US API for data and calculate EHP.

VERSION 1.1:

===UPDATES===

- You can now look up BattleTags with both '-' and '#' format. Example: Mytag#1234 and Mytag-1234 both work now.
- Class names are now presented next to character names during character selection.
- You can now select EU realm characters!
- Quick switch between characters in the EHP screen has been added. A drop-down at the top of the page allows you to switch.


===KNOWN ISSUES===

- Life regen is not yet taken into account. Getting a value for this requires many requests to the Diablo 3 profile API, and applications are limited in the amount of requests they can make.
- Bonuses due to sockets (such as LoH bonuses in weapons) are not taken into account. The Diablo 3 API has a bug here. This calculator will be fixed as soon as blizzard fixes the Diablo 3 API.